/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import React, {createRef, useEffect} from 'react';
import {
  LogBox,
  Platform,
  StatusBar,
  View,
  Image,
} from 'react-native';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import NavigatorComponent from './src/Navigations/Navigatore';
import {isReady, navigationRef} from './src/Navigations/RootNavigation';
import {persistor, store} from './src/Store/configureStore';
import {Colors, Images} from './src/Theme';
import SplashScreen from 'react-native-splash-screen'

export const alertRef = createRef();
LogBox.ignoreLogs(['Warning: ...']); // Ignore log  for not affect in feature
LogBox.ignoreAllLogs(); //Ignore non-affected log
const App = () => {

  useEffect(() => {
    // axios.defaults.baseURL = Config.baseURL;
    // axios.defaults.headers = Config.headers;
    // axios.defaults.timeout = Config.timeOut;
    SplashScreen.hide();
  }, []);

  return (
    <GestureHandlerRootView style={{flex: 1}}>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <NavigationContainer ref={navigationRef}>
            <View style={{flex: 1}}>
              <StatusBar
                backgroundColor={
                  Platform.OS === 'android' ? Colors.transparent : Colors.white
                }
                barStyle="dark-content"
                translucent={Platform.OS === 'android' ? true : false}
              />
              {/* <ThemeAlert ref={alertRef} /> */}
              {<NavigatorComponent />}
            </View>
          </NavigationContainer>
        </PersistGate>
      </Provider>
    </GestureHandlerRootView>
  );
};

export default App;
