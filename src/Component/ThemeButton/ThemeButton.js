import React, {Component} from 'react';
import {TouchableOpacity, StyleSheet, Text} from 'react-native';
// Mic
import {Colors, Fonts, Responsive} from '../../Theme';
import PropTypes from 'prop-types';

export default class ThemeButton extends Component {
  render() {
    const {textStyle = {}, onPress, textValue, disabled = false} = this.props;
    return (
      <TouchableOpacity
        activeOpacity={0.9}
        disabled={disabled}
        style={[
          styles.buttonStyle,
          this.props.containerStyle,
          disabled && {backgroundColor: Colors.grayShadeC0},
        ]}
        onPress={onPress}>
        <Text style={[styles.textStyle, textStyle]}>{textValue}</Text>
      </TouchableOpacity>
    );
  }
}

ThemeButton.propTypes = {
  textValue: PropTypes.any,
  textStyle: PropTypes.any,
  onPress: PropTypes.any,
  disabled: PropTypes.boolean,
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  buttonStyle: {
    width: '80%',
    height: Responsive.heightPercentageToDP(60),
    backgroundColor: Colors.pinkDark,
    alignSelf: 'center',
    borderRadius: Responsive.widthPercentageToDP(50),
    justifyContent: 'center',
  },
  textStyle: {
    textAlign: 'center',
    color: Colors.white,
    fontSize: Responsive.convertFontScale(16),
    fontWeight: '700',
    fontFamily: Fonts.fonts.ThemeRegular,
  },
});
