import React, {PureComponent} from 'react';
import {View, Modal, ActivityIndicator} from 'react-native';
import PropTypes from 'prop-types';
import {Colors} from '../Theme';

class Loader extends PureComponent {
  render() {
    const {isLoading} = this.props;
    return (
      <Modal visible={isLoading} transparent>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: Colors.shadowColor,
          }}>
          <ActivityIndicator
            style={{alignSelf: 'center'}}
            size="large"
            color={Colors.ThemeColorBlue}
          />
        </View>
      </Modal>
    );
  }
}
Loader.propTypes = {
  isSplash: PropTypes.bool,
  isLoading: PropTypes.bool.isRequired,
  isBiometricCheck: PropTypes.bool,
};

Loader.defaultProps = {
  isSplash: false,
  isLoading: false,
  isBiometricCheck: false,
};

export default Loader;
