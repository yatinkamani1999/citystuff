/* eslint-disable prettier/prettier */
import { I18nManager, InteractionManager } from 'react-native';
import { Constant } from '../../Theme';
import ReactNative from 'react-native';

import I18n from 'react-native-i18n';


// Import all locales
import en from './en.json';
import hi from './hi.json';
import AsyncStorage from '@react-native-async-storage/async-storage';

// Should the app fallback to English if user locale doesn't exists
I18n.fallbacks = true;

// Define the supported translations
I18n.translations = {
  en,
  hi,
};

const currentLocale = I18n.currentLocale();

// Is it a RTL language?
export const isRTL = currentLocale.indexOf('es') === 0;

// Allow RTL alignment in RTL languages
ReactNative.I18nManager.allowRTL(isRTL);

// The method we'll use instead of a regular string
export function strings(name, params = {}) {
  return I18n.t(name, params);
}

export function setAppLanguage(language) {
  let lng = Constant.commonConstant.english;
  if (language.includes(Constant.commonConstant.hindi)) {
    lng = Constant.commonConstant.hindi;
  }
  I18n.locale = lng;
  console.log(lng)
}

export async function changeLanguage(language) {
  // const props = this.props
//   Constant.commonConstant.isLanguageChanging = true;
  await AsyncStorage.setItem(Constant.asyncStorageKeys.languageCode, language).then(() => {
    setTimeout(() => {
      I18n.locale = language;
    //   Constant.commonConstant.currentLanguage = language;
    //   Constant.commonConstant.isLanguageChanging = false;
      // ReactNative.I18nManager.forceRTL(language === Constants.language.gujarati)
    //   RNRestart.Restart();
    }, 300);
  });
}
export default I18n;
