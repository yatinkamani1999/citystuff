export const validateEmail = email => {
  // let re =
  //   /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  let re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
  return re.test(email);
};

export const validatePassword = password => {
  return password != '' && password.length >= 8;
};

export const validateName = name => {
  var re = new RegExp(/^([a-zA-Z ]{1,})$/);
  return re.test(name);
};

export const validateTelephone = name => {
  // var re = new RegExp(/^[0]?[789]\d{9}$/);
  var re = new RegExp(/^\d{10}$/);
  return re.test(name);
};

export default {
  validateEmail,
  validateTelephone,
  validatePassword,
  validateName,
};
