import React from "react";
import Share from "react-native-share";
import Config from "../Config";
import firebase from '@react-native-firebase/dynamic-links';

interface TProps {
    title?: string,
    message?: string,
    userId?: string,
    productId?: string
} 

const ShareItem = async ({title, message, userId, productId}: TProps) => {

  console.log('============================', title, message, userId, productId)
  try {
    // const uri = await downloadAsync(url, cacheDirectory + "tmp.png");

    const link = await firebase().buildLink({
      link: Config.shareUrl + userId + "/" + productId ,
      domainUriPrefix: 'https://kambio.page.link',
      android: {
        packageName: 'com.kambiotodo',
        minimumVersion: '1',
      }
    })
      console.log('firebase link', link)
    const result = await Share.open(
      {
        title: 'Kambiotodo: \n' + title + '\n',
        url : link ,
        message: 'Kambiotodo: \n ' + title + '\n',
      });
    if (result.action === Share.sharedAction) {
      // console.log('=====================<><><>', result);
      // Always work with android
      if (result.activityType) {
        // worked
      } else {
        // shared
      }
    } else if (result.action === Share.dismissedAction) {
      // run only for ios if share is dismissed
      // console.log('=====================<><><>', result);
    }
  } catch (error) {
    console.log(error);
  }

}

export default ShareItem;
