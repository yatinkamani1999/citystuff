/* eslint-disable prettier/prettier */
import {Dimensions} from 'react-native';
import {EventEmitter} from 'fbemitter';
import Config from '../Config';

import DeviceInfo from 'react-native-device-info';
import {getUserToken} from './AsyncStorage';

// import { strings } from 'Resources/locales/i18n';
const {width, height} = Dimensions.get('window');

export const commonConstant = {
  scrWidth: width,
  scrHeight: height,
  userToken: '',
  emitter: new EventEmitter(),
  appUser: null,
  UserId: '',
  userLoginData: null,
  hasNotch: DeviceInfo.hasNotch(),
  isConnected: true,
  fcmToken: '',
  english: 'en',
  hindi: 'hi',
  franch: 'fr',
  currentLanguage: 'en',
  interval: null
};

export const eventListenerKeys = {
  Login: 'Login',
  Logout: 'Logout',
  Like: 'SuperLike',
};

export const asyncStorageKeys = {
  UserData: 'UserData',
  UserId: 'UserId',
  UserToken: 'Token',
  userLoginData: 'LoginData',
  language: 'Language',
  languageCode: 'languageCode',
};

export const getHeaderWithAuthToken = async () => {
  let token = await getUserToken();
  if (token === undefined || token === null) {
    return null;
  }

  const newHeaders = Object.assign({}, Config.headers, {
    'Authorization': `Bearer ${token}`,
  });

  return newHeaders;
};


export const API = {
  // LoginApi: '/api/authaccount/login',
  LoginApi: '/api/login',
  Logout: '/api/logout',
  RegisterApi: '/api/register',
  ForgotePasswordApi: '/api/sendResetLinkEmail',
  UpdateProfileApi: '/api/profileupdate',
  CountryList: '/api/country_list',
  getprofile: '/api/getprofile',
  addProduct: '/api/addproduct',
  editproduct: '/api/editproduct',
  getproductlisting: '/api/getproductlisting',
  faq: '/api/get_faq',
  getproductdetail: '/api/getproductdetail',
  getuserlistproductslike: '/api/getuserlistproductslike',
  getOtherProfileListing: '/api/getotherprofilelisting',
  get_message_list: '/api/get_message_list',
  get_message: '/api/get_message',
  send_message: '/api/send_message',
  delete_block_chat: '/api/delete_block_chat',
  deletesuperlikerequest: '/api/deletesuperlikerequest',
  user_ratings: '/api/user_ratings',
  reset_password: '/api/reset_password',
  flaginappropriate: '/api/flaginappropriate', 
  deleteproduct: '/api/deleteproduct', 
  deleteuser: '/api/user_delete', 
  deleteproductimage: '/api/deleteproductimage', 
  productsuperlike: '/api/productsuperlike', 
  userlike: '/api/userlike', 
  getsuperlikeslatest: '/api/getsuperlikeslatest', 
  savetoken: '/api/savetoken',
  getnotificationlist: '/api/getnotificationlist',
  readnotification: '/api/readnotification',
  getunreadcount: '/api/getunreadcount',
  checkProductDeleted: '/api/checkProductDeleted',
  productExchangedUserStore: '/api/productExchangedUserStore',
  productExchangedUserCheck: '/api/productExchangedUserCheck',
  userStatistics: '/api/user_statistics',

  CreatePaymentProfileApi: '/investors/create-payment-profile',
  ProfileAPi: '/investors/me',
  // register: '/api/authaccount/registration',
  register: '/investors/register',
  otp: '/api/check-otp',
  resetPwd: '/api/reset-password',
  changePwd: '/investors/change-password',
  updateProfile: '/api/update-profile',
  productNotification: '/report/by-product',
  reportByInvestore: '/report/by-investor',
  readReport: '/report/mark-read',
  progress: '/progress',
  myInvestment: '/services/invest2exit/investments/me',
  allIdeas: '/services/invest2exit/ideas',
  allSchemas: '/services/invest2exit/schemas',
};

export default {
  commonConstant,
  API,
  eventListenerKeys,
  asyncStorageKeys,
  getHeaderWithAuthToken
};
