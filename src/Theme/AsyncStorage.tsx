import AsyncStorage from '@react-native-async-storage/async-storage';
import Constant from './Constant';

export const StoreLanguage = language => {
  AsyncStorage.setItem(Constant.asyncStorageKeys.language, language);
};

export const getStoreLanguage = async () => {
  return await AsyncStorage.getItem(Constant.asyncStorageKeys.language);
};

export const setLogin = isLogin => {
  AsyncStorage.setItem('isLogin', '' + isLogin);
};

export const isLogin = async () => {
  let d = await AsyncStorage.getItem('isLogin');
  console.log('IsLogin==>', d);
  return d === 'true';
};

export const storeUserToken = async token => {
  let d = await AsyncStorage.setItem('token', token);
};

export const getUserToken = async () => {
  let d = await AsyncStorage.getItem('token');
  return d;
};

export const storeFCMToken = async token => {
  let d = await AsyncStorage.setItem('fcmToken', token);
};

export const getFCMToken = async () => {
  let d = await AsyncStorage.getItem('fcmToken');
  return d;
};

export const setNotificationSound = value => {
  AsyncStorage.setItem('notificationSound', String(value));
};

export const getNotificationSound = async () => {
  let d = await AsyncStorage.getItem('notificationSound');
  return d === 'true';
};

export const setNotificationVibrate = value => {
  AsyncStorage.setItem('notificationVibrate', String(value));
};

export const getNotificationVibrate = async () => {
  let d = await AsyncStorage.getItem('notificationVibrate');
  return d === 'true';
};

export const setGeneralNotification = value => {
  AsyncStorage.setItem('GeneralNotification', String(value));
};

export const getGeneralNotification = async () => {
  let d = await AsyncStorage.getItem('GeneralNotification');
  return d === 'true';
};

export const setSpecialOfferStore = value => {
  AsyncStorage.setItem('SpecialOffes', String(value));
};

export const getSpecialOfferStore = async () => {
  let d = await AsyncStorage.getItem('SpecialOffes');
  return d === 'true';
};

export const setAppUpdateStore = value => {
    AsyncStorage.setItem('AppUpdate', String(value));
  };
  
export const getAppUpdateStore = async () => {
  let d = await AsyncStorage.getItem('AppUpdate');
  return d === 'true';
};

export const setFirstTimeOpen = value => {
  AsyncStorage.setItem('isFirtTimeOpen', String(value));
};

export const isFirstTimeOpen = async () => {
  let d = await AsyncStorage.getItem('isFirtTimeOpen');
  return d === 'true';
};