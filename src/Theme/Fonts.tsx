const fonts = {
  ThemeBlack: 'Mulish-Black',
  ThemeBlackItalic: 'Mulish-BlackItalic',
  ThemeBold: 'Mulish-Bold',
  ThemeBoldItalic: 'Mulish-BoldItalic',
  ThemeExtraBold: 'Mulish-ExtraBold',
  ThemeExtraBoldItalic: 'Mulish-ExtraBoldItalic',
  ThemeExtraLight: 'Mulish-ExtraLight',
  ThemeExtraLightItalic: 'Mulish-ExtraLightItalic',
  ThemeItalic: 'Mulish-Italic',
  ThemeLight: 'Mulish-Light',
  ThemeLightItalic: 'Mulish-LightItalic',
  ThemeMedium: 'Mulish-Medium',
  ThemeMudiumItalic: 'Mulish-MediumItalic',
  ThemeRegular: 'Mulish-Regular',
  ThemeSemiBold: 'Mulish-SemiBold',
  ThemeSemiBoldItalic: 'Mulish-SemiBoldItalic',
};
export default {
  fonts,
};
