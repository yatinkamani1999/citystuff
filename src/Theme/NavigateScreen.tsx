import {
  CommonActions,
  DrawerActions,
  StackActions,
  useNavigationState,
} from '@react-navigation/native';
import React, {useEffect} from 'react';
import { navigationRef } from '../Navigations/RootNavigation';

// export const navigationRef = React.createRef();

// const routesLength = useNavigationState(state => state.routes.length);

export const push = (destinationScene, props) => {
  console.log('push', destinationScene, props, navigationRef.current);

  const routes = navigationRef.current.getState()?.routes;
  if (routes?.length > 0) {
    let ind = routes.findIndex(iten => iten.name === destinationScene);
    let data = routes.filter((item, index) => index <= ind);
    let routesData = [];
    data.map((item, index) => {
      if (index === data.length - 1) {
        routesData.push({name: item.name, params: props});
      } else {
        let data = {name: item.name, params: props};
        routesData.push(data);
      }
    });
    console.log('push', ind, routesData);
    if (ind >= 0) {
      const pushAction = CommonActions.reset({
        index: ind,
        routes: routesData,
      });
      return navigationRef.dispatch(pushAction);
    } else {
      const pushAction = StackActions.push(destinationScene, props);
      return navigationRef.current.dispatch(pushAction);
    }
  } else {
    const pushAction = StackActions.push(destinationScene, props);
    return navigationRef.current.dispatch(pushAction);
  }
 
};

export const pushScreen = (destinationScene, props) => {
  const pushAction = StackActions.push(destinationScene, props);
    return navigationRef.current.dispatch(pushAction);
}

// Pop to last navigate key.
export const pop = () => {
  // const popAction = StackActions.pop(1);
  // return navigationRef.current.dispatch(popAction);
  navigationRef.dispatch(CommonActions.goBack());
  // return navigationRef.current.goBack();
  console.log('=====------>', 'Go Back');
};

export const replace = (destinationScene, props) => {
  console.log('push', destinationScene, props);
  const replaceActionn = StackActions.replace(destinationScene, props);
  return navigationRef.current.dispatch(replaceActionn);
};

export const reset = (destinationScene, props) => {
  console.log(destinationScene, props);
  
  const pushAction = CommonActions.reset({
    index: 0,
    routes: [
      {
        name: destinationScene,
        params: props,
      },
    ],
  });
  return navigationRef.current.dispatch(pushAction);
};

export const jumpToTabs = (tabs, destinationScene, props) => {
  return navigationRef.current.navigate(tabs, {
    screen: destinationScene,
    params: props,
  });
};

export const drawerToggle = () => {
  navigationRef.current.dispatch(DrawerActions.toggleDrawer());;
}