/* eslint-disable prettier/prettier */
import React, {} from 'react';
import {
  TouchableOpacity,
  View,
  Text,
} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

// Lib
import {Screens, Colors, Responsive, Constant} from '../Theme';

// Login and signUp flow Screen
import SplashScreenView from '../Container/SplashScreen/SplashScreen';


import {
  CardStyleInterpolators,
  createStackNavigator,
} from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import OnBoradingScreen from '../Container/SplashScreen/OnBoradingScreen/OnBoradingScreen';
import HomeScreen from '../Container/MainScreen/Home/HomeScreen';
import DetailsScreen from '../Container/MainScreen/Details/DetailsScreen';

const onBackPress = () => {
  // try{
  // BackHandler.exitApp();
  //   Actions.pop();
  // }catch(e){
  // }
};

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();


const DrawerNav = () => {
  return (
    <Drawer.Navigator
      screenOptions={{
        headerShown: false,
      }}
      drawerContent={props => <DrawerView {...props} />}>
      <Drawer.Screen
        options={{
          headerShown: false,
        }}
        component={HomeScreen}
        name={Screens.HomeScreen}
      />
    </Drawer.Navigator>
  );
};


const DrawerView = () => {
  return (
    <View
      style={{
        marginHorizontal: Responsive.moderateScale(10),
        marginVertical: Responsive.moderateScale(40),
      }}>
      <TouchableOpacity
        onPress={() => {
          // onLogout();
        }}
        style={{
          width: '100%',
          backgroundColor: 'rgba(220,222,220,0.5)',
          borderRadius: Responsive.moderateScale(10),
          marginVertical: Responsive.moderateScale(20),
          paddingVertical: Responsive.moderateScale(10),
          paddingHorizontal: Responsive.moderateScale(15),
        }}>
        <Text
          style={{color: Colors.red, fontSize: Responsive.moderateScale(16)}}>
          Logout
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const forFade = ({current}) => ({
  cardStyle: {
    opacity: current.progress,
  },
});

const transitionConfig = {
  transitionSpec: {
    open: {
      animation: 'spring',
      config: {
        stiffness: 50,
        damping: 110,
        mass: 0.1,
        overshootClamping: false,
        restDisplacementThreshold: 10,
        restSpeedThreshold: 10,
      },
    },
    close: {
      animation: 'spring',
      config: {
        stiffness: 50,
        damping: 10,
        mass: 0.1,
        overshootClamping: false,
        restDisplacementThreshold: 10,
        restSpeedThreshold: 10,
      },
    },
  },
};

// Main Navigation Flow
const NavigatorComponent = () => {
  const userData = Constant.commonConstant.appUser;

  return (
    <Stack.Navigator
      key={Screens.AuthKey}
      screenOptions={{
        headerShown: false,
        gestureEnabled: false,
        cardStyleInterpolator: forFade,
      }}>
      <Stack.Screen
        options={{
          cardStyleInterpolator:
            CardStyleInterpolators.forScaleFromCenterAndroid,
        }}
        key={Screens.SplashScreen}
        name={Screens.SplashScreen}
        component={SplashScreenView}
      />

      <Stack.Screen
        options={{
          cardStyleInterpolator:
            CardStyleInterpolators.forScaleFromCenterAndroid,
        }}
        key={Screens.OnBoardingScreen}
        name={Screens.OnBoardingScreen}
        component={OnBoradingScreen}
      />

      <Stack.Screen
        options={{
          cardStyleInterpolator:
            CardStyleInterpolators.forScaleFromCenterAndroid,
        }}
        key={Screens.HomeScreen}
        name={Screens.HomeScreen}
        component={HomeScreen}
      />

      <Stack.Screen
        options={{
          cardStyleInterpolator:
            CardStyleInterpolators.forScaleFromCenterAndroid,
        }}
        key={Screens.DetailsScreen}
        name={Screens.DetailsScreen}
        component={DetailsScreen}
      />


    </Stack.Navigator>
  );
};

export default NavigatorComponent;
