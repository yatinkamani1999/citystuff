import { createNavigationContainerRef } from "@react-navigation/native";

export const navigationRef = createNavigationContainerRef();

export const isReady = () => {
  return new Promise((resolve) => {
    const inter = setInterval(() => {
      if (navigationRef.isReady()) {
        resolve(true);
        clearInterval(inter);
      }
    }, 1000);
  });
};

export function navigate(name, params) {
  if (navigationRef.isReady()) {
    navigationRef.navigate(name, params);
  }
}
