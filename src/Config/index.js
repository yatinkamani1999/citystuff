import {Constant} from '../Theme';

export default {
  baseURL: 'http://143.42.103.146',
  imageBaseURL: 'http://143.42.103.146/storage/avatars/',
  productImageBaseURL: 'http://143.42.103.146/storage/products/',
  socketUrl: 'http://143.42.103.146:3000/',
  shareUrl: 'http://www.kambiotodo.com/',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    // tenant: 'root',
    // Authorization: `Bearer ${Constant.commonConstant.userToken}`,
  },
  timeOut: 30000,
};
