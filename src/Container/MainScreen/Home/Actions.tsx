import {showAlert, showNetWorkAlert} from '../../../Functions/Alerts';
import {Constant} from '../../../Theme';

import axios from 'axios';
import { getHeaderWithAuthToken } from '../../../Theme/Constant';

export async function getProductDetail(id = 1) {
  if (Constant.commonConstant.isConnected) {
    return axios
      .get(Constant.API.getproductdetail+"?product_id="+id)
      .then(function (response) {
        return response;
      })
      .catch(function (error) {
        console.log('error ', error);
        return error.response;
      });
  } else {
    showNetWorkAlert();
    return null;
  }
}


export async function superLike(data) {
  if (Constant.commonConstant.isConnected) {
    return axios
      .post(Constant.API.productsuperlike, data)
      .then(function (response) {
        return response;
      })
      .catch(function (error) {
        console.log('error ', error);
        return error.response;
      });
  } else {
    showNetWorkAlert();
    return null;
  }
}

export async function userLike(data) {
  if (Constant.commonConstant.isConnected) {
    return axios
      .post(Constant.API.userlike, data)
      .then(function (response) {
        return response;
      })
      .catch(function (error) {
        console.log('error ', error);
        return error.response;
      });
  } else {
    showNetWorkAlert();
    return null;
  }
}

export async function userStatistics(data: any) {
  if (Constant.commonConstant.isConnected) {
    return axios
      .post(Constant.API.userStatistics, data)
      .then(function (response) {
        return response;
      })
      .catch(function (error) {
        console.log('error ', error);
        return error.response;
      });
  } else {
    showNetWorkAlert();
    return null;
  }
}