import {Dimensions, Platform, StyleSheet} from 'react-native';
import {Colors, Constant, Fonts, Responsive} from '../../../Theme';

export const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: Colors.white,
    paddingTop: Responsive.heightPercentageToDP(40),
  },
  txtName:{ textAlign: 'center', fontSize: Responsive.convertFontScale(12), color: Colors.white, fontFamily: Fonts.fonts.ThemeRegular},
  tabView: (active: any) => (
    {
      paddingVertical: Responsive.heightPercentageToDP(10),
      borderRadius: 30,
      backgroundColor: active ? Colors.ThemeColor : Colors.grayShadeF0,
      paddingHorizontal: Responsive.widthPercentageToDP(20)
    }
  )
});
