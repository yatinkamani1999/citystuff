import React, {
  createRef,
  useState,
} from 'react';
import {
  FlatList,
  Image,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  UIManager,
  View,
} from 'react-native';
import {styles} from './HomeScreenStyles';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import Responsive, { SCREEN_WIDTH } from '../../../Theme/Responsive';
import { Colors, Images, Screens } from '../../../Theme';
import { navigate } from '../../../Navigations/RootNavigation';

if (
  Platform.OS === 'android' &&
  UIManager.setLayoutAnimationEnabledExperimental
) {
  UIManager.setLayoutAnimationEnabledExperimental(true);
}

export const LogoutRef = createRef();
const HomeScreen = props => {

  const [activeIndex, setActiveIndex] = useState(0);
  const [slider, setSlider] = useState([{name: 'yes'}, {name: 'yes'}, {name: 'yes'}, {name: 'yes'}]);

  const sliderView = () => {
    return (
      <View style={{marginVertical: 20}}>
        <Carousel
          ref={c => {
            c;
          }}
          data={slider}
          renderItem={({item, index}) => {
            return (
              <View
                style={{
                  width: SCREEN_WIDTH - Responsive.heightPercentageToDP(60),
                  height: Responsive.verticalScale(150),
                  backgroundColor: Colors.background,
                  borderRadius: Responsive.widthPercentageToDP(10),
                }}>
                <Text style={styles.txtName}>{item.name}</Text>
              </View>
            );
          }}
          onSnapToItem={index => setActiveIndex(index)}
          sliderWidth={SCREEN_WIDTH}
          loop={true}
          hasParallaxImages={true}
          itemWidth={SCREEN_WIDTH - Responsive.heightPercentageToDP(60)}
        />

        <View style={{marginTop: -Responsive.verticalScale(10)}}>
          <Pagination
            dotsLength={slider.length}
            inactiveDotOpacity={1}
            inactiveDotScale={1}
            activeDotIndex={activeIndex}
            // containerStyle={{backgroundColor: 'red'}}
            dotColor={Colors.primary}
            dotStyle={{width: Responsive.widthPercentageToDP(25)}}
            inactiveDotStyle={{width: 10}}
            inactiveDotColor={Colors.grayShadeE0}
          />
        </View>
      </View>
    );
  };

  const Toolbar = () => {
    return (
      <View style={{paddingVertical: 20, justifyContent: 'center', alignItems: 'center'}}>
        <Text style={{color: Colors.ThemeColor, fontSize: Responsive.moderateScale(18)}} >City Stuff</Text>
      </View>
    )
  }

  const Tabs = () => {

    const [selected, setSelected] = useState('All');

    return (

      <View style={{flexDirection: 'row', width: '90%', alignSelf: 'center', justifyContent: 'space-between'}}>
        {/* <ScrollView style={{ backgroundColor: 'red' }} horizontal> */}

          {['All', "Today", "Poopuler", "Deals"].map((_item) => {
            return (
              
            <TouchableOpacity onPress={() => setSelected(_item)} style={styles.tabView(selected === _item)}>
              <Text style={{ color: selected === _item ? Colors.white : Colors.pinkDark, fontSize: Responsive.moderateScale(14) }}>{_item}</Text>
            </TouchableOpacity>

            );
          })}

        {/* </ScrollView> */}
      </View>
    )
  }

  const Product = () => {

    return(
      <View style={{width: '90%', marginVertical: 20, alignSelf: 'center'}}>

        <FlatList
          data={['', '', '', '', '']}
          ItemSeparatorComponent={(item) => <View style={{height: 30}} />}
          renderItem={({item, index}: any) => {
            return(
              <TouchableOpacity onPress={() => navigate(Screens.DetailsScreen, item)} style={{borderRadius: 30, height: 200, width: '100%'}}>
                <Image style={{ width: '100%', height: '100%' , borderRadius: 20}} source={Images.slide1} resizeMode={'contain'} />
                <Text style={{position: 'absolute', fontSize: Responsive.scale(16), color: Colors.white, bottom: 10, left: 20}} >{'Insable Shoes'}</Text>
              </TouchableOpacity>
            )
          }}
        />

        

      </View>
    )

  }

  return (
    <View style={styles.mainContainer}>
      <Toolbar />
      <ScrollView>
        {sliderView()}
        <Tabs />
        <Product />
      </ScrollView>
    </View>
  );
};

export default HomeScreen;
