import React, { useEffect, useRef, useState } from 'react';
import { ActivityIndicator, Text, View, StyleSheet, Animated, Image, Easing } from 'react-native';
// Mics
import { Colors, Images, Responsive, Fonts, Screens } from '../../../Theme';

import AppIntroSlider from 'react-native-app-intro-slider';
import ThemeButton from '../../../Component/ThemeButton/ThemeButton';
import { replace } from '../../../Theme/NavigateScreen';

const slides = [
    {
        key: 1,
        image: Images.slide2,
    },
    {
        key: 2,
        image: Images.slide3,
    }
];

const OnBoradingScreen = props => {
    const [isLoading, setIsLoading] = useState(false);

    let timeOuter = null;
    const slideRef = useRef(null);

    const top = new Animated.Value(-100);
    const left = new Animated.Value(-50);

    useEffect(() => {
        timeOuter = setTimeout(() => {
        }, 1500)

        let listner = props.navigation.addListener('blur', () => {
            // do some API calls here
            clearTimeout(timeOuter);
            console.log('listener section');
        });

        return () => {
            listner();
        }
        
    }, []);

    useEffect(() => {
        Animated.timing(top, {
            toValue: 0,
            duration: 1000,
            easing: Easing.inOut(Easing.ease),
            useNativeDriver: false
        }).start();
        Animated.timing(left, {
            toValue: 0,
            duration: 1000,
            easing: Easing.inOut(Easing.ease),
            useNativeDriver: false
        }).start();
    }, []);

    if (isLoading == true) {
        return (
            <ActivityIndicator
                style={styles.spinner}
                size="large"
                color={Colors.ThemeColor}
            />
        );
    }

    const _renderItem = ({ item }) => {
        return (
            <View style={styles.slide}>
                <Image style={styles.img} resizeMode={'contain'} source={item.image} />
            </View>
        );
    }

    return (
        <View style={styles.container}>

            <View style={styles.view}>
                <AppIntroSlider
                    ref={slideRef}
                    data={slides}
                    renderItem={_renderItem}
                    showDoneButton={false}
                    showPrevButton={false}
                    showSkipButton={false}
                    showNextButton={false}
                    activeDotStyle={{ backgroundColor: Colors.pinkDark }}
                />
            </View>

            <Animated.View style={{ width: '80%', top: top, right: left }}>
                <Text style={styles.title}>{'Uncover the Best Urban Offers'}</Text>
                <Text style={styles.titleContent}>{'CityStuff is your ticket to unlocking exclusive offers and deals in your city! Say goodbye to overspending and hello to savings with our curated selection of the best discounts, promotions, and special deals available right at your fingertips'}</Text>
            </Animated.View>

            <ThemeButton
                containerStyle={styles.signupContainer}
                textStyle={styles.signupText}
                textValue={'Get Started'}
                onPress={() => replace(Screens.HomeScreen)}>
            </ThemeButton>
        </View >
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    view:{ height: '65%', width: '100%', marginBottom: Responsive.heightPercentageToDP(20) },
    slide: {
        alignItems: 'center',
    },
    img:{width: '100%', height: '100%'},
    background1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    logoStyle: {
        alignSelf: 'center',
        resizeMode: 'contain',
        height: Responsive.heightPercentageToDP(100),
        width: Responsive.widthPercentageToDP(250),
        marginTop: Responsive.heightPercentageToDP(20),
        marginBottom: Responsive.heightPercentageToDP(30),
    },
    title: {
        fontSize: Responsive.convertFontScale(20),
        // fontWeight: 'bold',
        color: '#364C4F',
        marginTop: 20,
        fontFamily: Fonts.fonts.ThemeBold,
        textAlign: 'center',
        marginBottom: 10,
        marginLeft: 20,
        marginRight: 20,
    },
    titleContent: {
        fontSize: Responsive.convertFontScale(12),
        // fontWeight: 'bold',
        color: '#364C4F',
        opacity: 0.5,
        fontFamily: Fonts.fonts.ThemeBold,
        textAlign: 'center',
        marginBottom: 20,
        marginLeft: 20,
        marginRight: 20,
    },
    loginContainer: {
        width: '70%',
        backgroundColor: Colors.tint,
        borderRadius: 30,
        padding: 10,
        marginTop: 30,
        color: Colors.tint,
    },
    loginText: {
        color: Colors.white,
        fontFamily: Fonts.fonts.ThemeExtraBold,
    },
    signupContainer: {
        width: '70%',
        height: Responsive.heightPercentageToDP(65),
        backgroundColor: Colors.pinkDark,
        borderRadius: 60,
        padding: 15,
        borderWidth: 0,
        borderColor: Colors.white,
        color: Colors.white,
        fontFamily: Fonts.fonts.ThemeRegular,
        marginTop: 15,
    },
    signupText: {
        color: Colors.white,
        fontFamily: Fonts.fonts.ThemeRegular,
    },
    spinner: {
        marginTop: 200,
    },
});

export default OnBoradingScreen;