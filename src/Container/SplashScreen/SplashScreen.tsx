import React from "react";
import { useState } from "react";
import { useEffect } from "react";
import { View, Image, StyleSheet, Animated } from 'react-native';
import { Colors, Images, Screens } from "../../Theme";
import { isFirstTimeOpen, isLogin, setFirstTimeOpen } from "../../Theme/AsyncStorage";
import { replace, reset } from "../../Theme/NavigateScreen";
import { useDispatch, useSelector } from "react-redux";

import { getHeaderWithAuthToken } from "../../Theme/Constant";
import axios from "axios";
import { socketConnect } from "../Socket";


// const permisionRequestion = async () => {
//   let permission =
//     Platform.OS === 'ios' 
//     ? [PERMISSIONS.IOS.LOCATION_ALWAYS, PERMISSIONS.IOS.LOCATION_WHEN_IN_USE] 
//     : [PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION, PERMISSIONS.ANDROID.ACCESS_COARSE_LOCATION];
//   const res = await checkMultiple(permission);

//   if (Platform.OS === 'android'){
//     // if ( res["android.permission.ACCESS_COARSE_LOCATION"] === RESULTS.GRANTED && res["android.permission.ACCESS_FINE_LOCATION"] == RESULTS.GRANTED){
//     // } else
//      if ( res["android.permission.ACCESS_COARSE_LOCATION"] === RESULTS.DENIED || res["android.permission.ACCESS_FINE_LOCATION"] == RESULTS.DENIED){
//       const res2 = await requestMultiple(permission);
//       console.log('============>', res2);
//     }
//   } else {
//     if ( res["ios.permission.LOCATION_ALWAYS"] === RESULTS.DENIED || res["ios.permission.LOCATION_WHEN_IN_USE"] == RESULTS.DENIED){
//       const res2 = await requestMultiple(permission);
//       console.log('============>', res2);
//     }
//   }
//   console.log('============>', res);

// };

const SplashScreenView = () => {

  useEffect(() => {
    handleBoarding();
  });
  
  const handleBoarding = async () => {

    const isFirstTime = await isFirstTimeOpen();

    if(!isFirstTime){
      setFirstTimeOpen(true);
      replace(Screens.OnBoardingScreen);
    } else {
      replace(Screens.HomeScreen);
    }

  };

  return (
    <View style={styles.container}>
      <Image source={Images.icon} style={styles.img} />
    </View>
  );

}

const styles = StyleSheet.create({
  containers: {
    flex: 1,
  },
  img:{ position: 'absolute', height: '10%', width: '80%', resizeMode: 'contain' },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.background,
  },
})

export default SplashScreenView;