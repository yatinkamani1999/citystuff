import {io} from 'socket.io-client';
import Config from '../Config';
export const Socket = {
  socket: io(''),
};

export const socketConnect = id => {
  Socket.socket = io(Config.socketUrl, {query: {userId: id}});
};
