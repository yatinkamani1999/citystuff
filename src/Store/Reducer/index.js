import { combineReducers } from "redux";
import LoginReducerSliceAction from "./LoginReducerSliceAction";

export const Reducers = combineReducers({
    userData: LoginReducerSliceAction,
})