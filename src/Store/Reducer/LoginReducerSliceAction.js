import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  //some data
  userData: null
};
const userSlice = createSlice({
  name: 'userData',
  initialState,
  reducers: {
    setUserData: (state, action) => {
      console.log('setUserData Data', state)
      state.userData = action.payload;
    },
    onLogin: (state, action)  => {},
    getUserData: () => {},
    setError: (state, action) => {
      state.userData = action.payload;
      state.isLoading = false;
    },
    clearLoginData: (state, action) => {
      console.log('Clear Data', state)
      state.userData = null;
      state.isLoading = false;
    }
  },
});
export const userSliceActions = userSlice.actions;
export default userSlice.reducer;