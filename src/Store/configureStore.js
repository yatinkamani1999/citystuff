import { configureStore } from '@reduxjs/toolkit';

import {Reducers} from './Reducer';
// import thunk from 'redux-thunk';
// import logger from 'redux-logger';
import { createLogger } from 'redux-logger';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { persistReducer } from 'redux-persist';

import createSagakMiddleware from 'redux-saga';
import persistStore from 'redux-persist/es/persistStore';
const saga = createSagakMiddleware()
const middleware = [saga, createLogger()]

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  keyPrefix: '',
};

const rootReducer = persistReducer(persistConfig, Reducers);

// const store = createStore(persistedReducer, applyMiddleware(thunk, logger));
// Note : Please remove logger while genarating the build
// const store = createStore(persistedReducer, applyMiddleware(thunk));
export const store = configureStore({
  reducer: rootReducer,
  middleware: middleware,
})

// let persistor = (store);
export const persistor = persistStore(store)
